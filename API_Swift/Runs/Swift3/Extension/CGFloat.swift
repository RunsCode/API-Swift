//
//  CGFloat.swift
//  API_Swift
//
//  Created by Dev_Wang on 2017/5/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation


extension CGFloat {
    
    func pixel() -> CGFloat {
        var unit: Double = 0.0
        switch Int(UIScreen.main.scale) {
        case 1: unit = 1.0 / 1.0
        case 2: unit = 1.0 / 2.0
        case 3: unit = 1.0 / 3.0
        default: unit = 0.0
        }
        return roundbyunit(&unit)
    }
    
    private func floorbyunit(_ unit: inout Double) -> CGFloat {
        let remain = modf(Double(self), &unit)
        if remain > (unit / 2.0) {
            return ceilbyunit(&unit)
        }
        return floorbyunit(&unit)
    }
    
    private func ceilbyunit(_ unit: inout Double) -> CGFloat {
        return CGFloat(Double(self) - modf(Double(self), &unit) + unit)
    }
    
    private func roundbyunit(_ unit: inout Double) -> CGFloat {
        return CGFloat(Double(self) - modf(Double(self), &unit))
    }
}
