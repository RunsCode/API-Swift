//
//  ImageView.swift
//  API_Swift
//
//  Created by Dev_Wang on 2017/5/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation

extension UIImageView {
    
    override func rs_corner(radius: Float, borderWidth: Float = 1.0, backgroundColor: UIColor = .clear, borderColor: UIColor = .black) {
        self.image = self.image?.rs_drawRectWithRoundedCorner(radius: CGFloat(radius), self.bounds.size)
    }

}
