//
//  TableView.swift
//  API_Swift
//
//  Created by Dev_Wang on 2017/5/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation


protocol ViewNameReusable:class { }

extension ViewNameReusable where Self:UIView {
    static var reuseIdentifier:String {
        return String(describing: self)
    }
}

extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ViewNameReusable {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
}
