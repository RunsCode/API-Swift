//
//  View.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/3/13.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation
import UIKit


//from CornerRadius KtCorner.swift
extension UIView {
    
    func rs_corner(radius: Float, borderWidth: Float = 1.0, backgroundColor: UIColor = .clear, borderColor: UIColor = .black) {
        
        let imageView = UIImageView(image: rs_drawRectWithRoundedCorner(radius: CGFloat(radius), borderWidth: CGFloat(borderWidth), backgroundColor: backgroundColor, borderColor: borderColor))
        self.insertSubview(imageView, at: 0)
    }

    
    func rs_drawRectWithRoundedCorner(radius: CGFloat, borderWidth: CGFloat, backgroundColor: UIColor, borderColor: UIColor) -> UIImage {
        
        let sizeToFit = CGSize(width: self.bounds.size.width.pixel(), height: self.bounds.size.height)
        let halfBorderWidth = CGFloat(borderWidth / 2.0)
        
        UIGraphicsBeginImageContextWithOptions(sizeToFit, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setLineWidth(borderWidth)
        context?.setStrokeColor(borderColor.cgColor)
        context?.setFillColor(backgroundColor.cgColor)
        
        let width = sizeToFit.width, height = sizeToFit.height
        context?.move(to: CGPoint(x: width - halfBorderWidth, y: radius + halfBorderWidth))
        //right_bottom
        context?.addArc(tangent1End: CGPoint(x: width - halfBorderWidth, y: height - halfBorderWidth), tangent2End: CGPoint(x: width - radius - halfBorderWidth, y: height - halfBorderWidth), radius: radius)
        //left_bottom
        context?.addArc(tangent1End: CGPoint(x: halfBorderWidth, y: height - halfBorderWidth), tangent2End: CGPoint(x: halfBorderWidth, y: height - radius - halfBorderWidth), radius: radius)
        //left_top
        context?.addArc(tangent1End: CGPoint(x: halfBorderWidth, y: halfBorderWidth), tangent2End: CGPoint(x: width - halfBorderWidth, y: halfBorderWidth), radius: radius)
        //right_top
        context?.addArc(tangent1End: CGPoint(x: width - halfBorderWidth, y: halfBorderWidth), tangent2End: CGPoint(x: width - halfBorderWidth, y: radius + halfBorderWidth), radius: radius)
        UIGraphicsGetCurrentContext()?.drawPath(using: .fillStroke)
        let output = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return output!
    }

}
