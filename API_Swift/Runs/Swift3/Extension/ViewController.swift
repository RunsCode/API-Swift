//
//  ViewController.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/3/23.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func edgeLayoutNone() {
        self.edgesForExtendedLayout = .init(rawValue: 0)
    }
}

