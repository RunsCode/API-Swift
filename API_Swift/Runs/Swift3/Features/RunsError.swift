//
//  RunsError.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import UIKit

struct RunsError: RunsErrorProtocol {
    var errorCode: Int
    var description: String
    var userInfo: [AnyHashable: Any]
    
    init(code: Int = -1, desc: String = "No description (Unkonow)", info: [AnyHashable: Any] = [:]) {
        errorCode = code
        description = desc
        userInfo = info
    }
}

extension RunsError: Error {
    
}
