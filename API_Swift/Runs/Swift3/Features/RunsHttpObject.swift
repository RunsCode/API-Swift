//
//  RunsHttpObject.swift
//  QiuQiuGram
//
//  Created by Dev_Wang on 2016/10/27.
//  Copyright © 2016年 iOS. All rights reserved.
//

import UIKit

enum HttpJsonKey: String {
    case content = "Content"
    case message = "Message"
    case data    = "Data"
    case code    = "Code"
    case errCode = "ErrCode"
    case null    = "nil"
    case error   = "error"
}

class RunsHttpObject: NSObject {
    public var respCode: Int = DefaultInt
    public var respMsg: String = DefaultString
    public var respDesc: String = DefaultString
    public var content: String = DefaultString
    public var isSuccess: Bool = false
    public var isValid: Bool = false
    public var data: Any?
    public var originData: [String:AnyObject]?
    public var model: AnyObject?
    public var cacheToken: String = DefaultString
    
    convenience init(_ jsonMap: [String:AnyObject]) {
        self.init()
        self.originData = jsonMap
        self.json()
        
        if jsonMap.keys.contains(HttpJsonKey.message.rawValue) {
            self.respMsg = (jsonMap[HttpJsonKey.message.rawValue] as? String) ?? DefaultString
        }
        
        if jsonMap.keys.contains(HttpJsonKey.code.rawValue) {
            if let number: NSNumber = jsonMap[HttpJsonKey.code.rawValue] as? NSNumber {
                self.respCode = number.intValue
            }
            self.isSuccess = self.respCode == 0
        }
        
        if jsonMap.keys.contains(HttpJsonKey.data.rawValue) {
            
            if let dataMap: [String: AnyObject] = jsonMap[HttpJsonKey.data.rawValue] as? [String : AnyObject] {
                self.data = dataMap
            }else if let jsonString: String = jsonMap[HttpJsonKey.data.rawValue] as? String {
                self.data = RunsHttpObject.convertJsonObject(with: jsonString)
            }
            self.isValid = (self.data != nil && self.isSuccess)
        }
        
        if jsonMap.keys.contains(HttpJsonKey.content.rawValue) {
            if let content = jsonMap[HttpJsonKey.content.rawValue] as? String{
                self.content = content
                self.isSuccess = true
            }
        }
    }
    
    convenience init(error: NSError? = NSError(domain: "", code: 1, userInfo: nil)) {
        self.init()
        self.isSuccess = false
        self.respCode = (error?.code)!
        self.respDesc = (error?.localizedDescription)!
    }
    
    private func json() {
        #if DEBUG
            if !IS_DATA_TEST {
                return
            }
//            let json = RunsHttpObject.convertJsonString(with: self.originData)
//            PPCLIENT.sendNotification(kShowHttpJsonDataMessage, body: json)
        #endif
    }
}

extension RunsHttpObject {
    
    /// 原始数据数据转换适配到json
    ///
    /// - Parameter respone: 原始返回
    /// - Returns: json
    static func conversionAdaption(with respone: Any) -> [String:Any] {
        if respone is Dictionary<String, Any> { return respone as! [String : Any] }
        if respone  is String { return [HttpJsonKey.content.rawValue: respone] }
        
        if respone is Data {
            let result = deserializedJson(with: respone)
            guard nil != result.json else {
                guard let content = String(data: respone as! Data, encoding: .utf8) else {
                    return [HttpJsonKey.error.rawValue:" Data: cast to string error: \(String(describing: result.error))"]
                }
                //
                return [HttpJsonKey.content.rawValue:content]
            }
            return result.json!
        }
        
        if respone is InputStream {
            let result = deserializedJson(with: respone)
            guard nil != result.json else {
                var buffer = Array<UInt8>(repeating: 0, count: 1024)
                let bytesRead = (respone as! InputStream).read(&buffer, maxLength: 1024)
                guard  bytesRead > 0 else {
                    return [HttpJsonKey.error.rawValue:"Inputstream: cast to bytes error: \(String(describing: result.error))"]
                }
                //
                guard let json = String(bytesNoCopy: &buffer, length: bytesRead, encoding: .utf8, freeWhenDone: true) else {
                    return [HttpJsonKey.error.rawValue:"Inputstream: bytes cast to String error: \(String(describing: result.error))"]
                }
                //
                return [HttpJsonKey.content.rawValue:json]
            }
            return result.json!
        }
        return [HttpJsonKey.null.rawValue:HttpJsonKey.null.rawValue]
    }
    
    /// 反序列化成json
    ///
    /// - Parameter respone: 原始数据 (Data or InputStream)
    /// - Returns: 反序列化后的json 以及反序列化过程中的异常
    static func deserializedJson(with respone: Any) -> (json: [String:Any]?, error: Error?) {
        var obj: Any?, err: Error?
        if let data = respone as? Data {
            do {
                obj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            } catch  {
                err = error
            }
        }else if let inputStream = respone as? InputStream {
            do {
                obj = try JSONSerialization.jsonObject(with: inputStream, options: .mutableContainers)
            } catch  {
                err = error
            }
        }
        guard let json = obj as? [String:Any] else {
            RunsLog("Inputstream or Data object cast to json error)")
            return (nil, err)
        }
        return (json, nil)
    }
    
    /// json对象转换成json字符串
    ///
    /// - Parameter jsonObject: json对象
    /// - Returns: json字符串
    static func convertJsonString(with jsonObject: Any) -> String? {
        var data: Data?
        do {
            data = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        } catch {
            RunsLog(error)
            return nil
        }
        if nil != data {
            guard let jsonString = String(data: data!, encoding: .utf8) else { return nil }
            return jsonString
        }
        return nil
    }
    
    /// json字符串转json对象
    ///
    /// - Parameter jsonString: json字符串
    /// - Returns: json对象 :Dictionary
    static func convertJsonObject(with jsonString: String) -> Any? {
        guard let data = jsonString.data(using: .utf8) else { return nil }
        var jsonObject: Any?
        do {
            jsonObject =  try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch  {
            RunsLog(error)
        }
        return jsonObject
    }
    
}

func NilInfo(_ object: RunsHttpObject, _ message: String,_ callback: RequestCallbackFunc?) {
    if nil != callback {
        callback!(object)
    }
//    let errMsg: String = "Http Error : null " + message + "\nerror : " + object.respMsg
//    PPCLIENT.sendNotification(kShowHttpJsonDataMessage, body: errMsg)
        RunsLog("Nil: \(message), errorCode : \(object.respCode), message : \(object.respMsg)")
    //    RunsDebugToast("Nil: \(message), errorCode : \(object.respCode), message : \(object.respMsg)")
}

func ErrorInfo(_ error: Error, _ message: String,_ callback: RequestCallbackFunc?) {
    let object = RunsHttpObject(error: error as NSError?)
    if nil != callback {
        callback!(object)
    }
    RunsLog("\(message) error : \(error)")
//    let errMsg: String = "Http Error : " + message + "\nerror : " + error.localizedDescription
//    PPCLIENT.sendNotification(kShowHttpJsonDataMessage, body: errMsg)
//    RunsDebugToast("\(message) error : \(error)")
}
