//
//  RunsLocation.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/2/7.
//  Copyright © 2017年 Giant. All rights reserved.
//

import UIKit
import CoreLocation

typealias LocationCallback = (_ entity: LocationStruct?) -> Void

struct LocationStruct {
    public var name: String = ""
    public var address: String = ""
    public var placemark: CLPlacemark?
}

final class RunsLocation: NSObject {
    
    fileprivate let latitube_offet = 0.002
    fileprivate let longitude_offet = 0.002
    
    fileprivate var lastLatitube: Double = 0.0
    fileprivate var lastLongitude: Double = 0.0
    
    fileprivate var mCallback: LocationCallback?
    fileprivate var defaultEntity: LocationStruct?
    
    public static let `default` = RunsLocation()
    private override init() {
        //
    }
    
    func startUpdatingLocation(_ callback: @escaping LocationCallback) {
        self.mCallback = callback
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        self.locationManager.stopUpdatingLocation()
    }
    
    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
}

extension RunsLocation: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let coordinate = locations.last?.coordinate
        let latitube = coordinate?.latitude
        let longitude = coordinate?.longitude
        
        guard (latitube != nil) && (longitude != nil) else {
            manager.stopUpdatingLocation()
            self.mCallback!(self.defaultEntity)
            return
        }
        
        if (abs(lastLatitube - latitube!) < latitube_offet) || (abs(lastLongitude - longitude!) < longitude_offet) {
            manager.stopUpdatingLocation()
            self.mCallback!(self.defaultEntity)
            return
        }
        
        self.lastLatitube = latitube!;
        self.lastLongitude = longitude!;
        RunsLog("经度:\(latitube!) -- 纬度:\(longitude!)")
        //
        let location = CLLocation(latitude: latitube!, longitude: longitude!)
        CLGeocoder().reverseGeocodeLocation(location) { [weak self] (_ array: [CLPlacemark]?, error :Error?) in
            guard nil != array else {
                self?.mCallback!(self?.defaultEntity)
                return
            }
            array?.forEach {
                let map = $0.addressDictionary
                let address = (map?["FormattedAddressLines"] as! [String]).last! as String
                let entity = LocationStruct(name: $0.name!, address: address, placemark: $0)
                guard ((self?.mCallback) != nil) else {
                    return
                }
                self?.mCallback!(entity)
                self?.defaultEntity = entity
            }
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        RunsLog("error :\(error)")
    }

}
