//
//  RunsObserver.swift
//  QiuQiuGram
//
//  Created by Dev_Wang on 2016/12/28.
//  Copyright © 2016年 iOS. All rights reserved.
//

import UIKit

typealias HandNotificationCallBack = (_ notification: NSNotification) -> Void

class RunsObserver: NSObject {
    private var listNotification: [NSNotification]?
    private var callback: HandNotificationCallBack?
//    private var isRunning

    deinit {
        listNotification?.removeAll()
        listNotification = nil
        callback = nil
    }
    
    override init() {
        super.init()
    }
    
    func shutdown() {
        self.unRegisterObserver()
    }
    
    func startUpListenNames(_ notificationNameList: [NSNotification.Name], event: @escaping HandNotificationCallBack) {
        var notifications = [NSNotification]()
        for name in notificationNameList {
            notifications.append(NSNotification(name: name, object: nil))
        }
        self.startUpListen(notifications, event)
    }
    
    func startUpListen(_ notificationList: [NSNotification], _ event: @escaping HandNotificationCallBack) {
        self.callback = event
        self.registerObserver(notificationList)
    }

    private func registerObserver(_ list: [NSNotification]) {
        self.unRegisterObserver()
        if list.count <= 0 {
            return
        }
        for notification in list {
            NotificationCenter.default.addObserver(self, selector: #selector(self.handNotification(notification:)), name: notification.name, object: notification.object)
        }
        self.listNotification = list
    }

    private func unRegisterObserver() {
        if self.listNotification == nil || (self.listNotification?.count)! <= 0 {
            return
        }
        
        for notification in self.listNotification! {
            NotificationCenter.default.removeObserver(self, name: notification.name, object: notification.object)
        }
    }

    @objc private func handNotification(notification: NSNotification) {
        if (callback != nil) {
            self.callback!(notification)
        }
    }

    
}
