//
//  RunsHttpSession.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/7.
//  Copyright © 2017年 Giant. All rights reserved.
//

import UIKit
import AFNetworking

typealias SuccessCallback = (URLSessionDataTask, Any) -> Void
typealias FailureCallback = (URLSessionDataTask?, Error) -> Void

final class RunsHttpSession/*: NSObject*/ {
    public static let `default` = RunsHttpSession()
    
    var isSupportHttps: Bool = false
    var timeoutInterval: Float = 10.0
    var securityPolicy: AFSecurityPolicy?
    //
    private init() {
        deploCarrierApi()
    }
    
    /// Deployment configuration Https required by the default certificate file is .cer and. Certificate
    /// Need to read the files inside the local bundle
    ///
    /// - Parameters:
    ///   - withCerName: Certificate name
    ///   - type: The certificate type is generally, default is `.cer`
    ///   - mode: PinningMode ,default is `certificate`
    func deployHttps(_ withCerName: String, type: String = "cer", mode: AFSSLPinningMode = .certificate) {
        guard let cerPath = Bundle.main.path(forResource: withCerName, ofType: type) else {
            isSupportHttps = false
            RunsTerLog("Bundle don't exist for the Https configuration certificate file: \(withCerName).\(type)")
            return
        }
        do {
            let cerData = try Data(contentsOf: URL(fileURLWithPath: cerPath))
            var cerSet = Set<Data>()
            cerSet.insert(cerData)
            securityPolicy = AFSecurityPolicy(pinningMode: mode)
            securityPolicy?.allowInvalidCertificates = true
            securityPolicy?.pinnedCertificates = cerSet
            securityPolicy?.validatesDomainName = false
        } catch  {
            RunsTerLog("Https certificate file failed to transfer data:\(error)")
        }
        isSupportHttps = true
    }
    
    /// Api basic configuration
    func deploCarrierApi() {
        if isSupportHttps && nil != securityPolicy {
            sessionManager.securityPolicy = securityPolicy!
        }
        sessionManager.requestSerializer = requestSerializer
        sessionManager.requestSerializer.httpShouldHandleCookies = true
        sessionManager.requestSerializer.timeoutInterval = TimeInterval(timeoutInterval)
        //
        sessionManager.responseSerializer = responeSerializer
        sessionManager.responseSerializer.acceptableContentTypes = acceptableContentTypes
    }
    
    /// Configure the request header settings
    /// But you must wait until the basic configuration is complete before setting the request header
    /// After `deploCarrierApi()`
    ///
    /// - Parameter headMap: Request header key map
    func deploHttpHead(_ headMap: [String:String]) {
        headMap.forEach { [weak self] (key: String, value: String) in
            self?.sessionManager.requestSerializer.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    public lazy var sessionManager: AFHTTPSessionManager = {
        return AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
    }()
    
    public lazy var requestSerializer: AFHTTPRequestSerializer = {
        return AFHTTPRequestSerializer()
    }()
    
    public lazy var responeSerializer: AFHTTPResponseSerializer = {
        return AFHTTPResponseSerializer()
    }()
    
    public lazy var encoder: RunsHttpRequestEncodeProtocol = {
        return RunsHttpRequestEncode()
    }()
    
    public lazy var decoder: RunsHttpResponeDecodeProtocol = {
        return RunsHttpResponeDecode()
    }()

    public lazy var acceptableContentTypes: Set<String> = {
        var set = Set<String>()
        let array = ["application/json", "text/json", "text/javascript", "text/plain"]
        array.forEach{ (value) in
            set.insert(value)
        }
        return set
    }()
}


extension RunsHttpSession {
    
    /// Creates and runs an `URLSessionDataTask` with a `GET` request.
    ///
    /// - Parameters:
    ///   - urlString: The URL string used to create the request URL.
    ///   - parameters: The parameters to be encoded according to the client request serializer.
    ///   - success: A block object to be executed when the task finishes successfully. This block has no return value and takes two arguments: the data task, and the response object created by the client response serializer.
    ///   - failure: A block object to be executed when the task finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two
    /// - Returns: URLSessionDataTask?
    @discardableResult
    func get(_ urlString: String,
             parameters: Any,
             success: @escaping SuccessCallback,
             failure: @escaping FailureCallback)
        -> URLSessionDataTask?
    {
        guard RunsNetIsReachable() else { return nil }
        let encodeParameters = encoder.encode(parameters, urlString)
        return sessionManager.get(urlString, parameters: encodeParameters, progress: nil, success: { [weak self] (task, respone) in
            guard let res = respone else {
                RunsMainThread {
                    failure(task, RunsError())
                }
                return
            }
            let json = RunsHttpObject.conversionAdaption(with:res)
            let map = self?.decoder.decode(json, urlString)
            RunsMainThread {
                success(task, map as Any)
            }
        }) { (task, error) in
            RunsMainThread {
                failure(task, error)
            }
        }
    }
    
    /// Creates and runs an `URLSessionDataTask` with a `POST` request.
    ///
    /// - Parameters:
    ///   - urlString: The URL string used to create the request URL.
    ///   - parameters: The parameters to be encoded according to the client request serializer.
    ///   - success: A block object to be executed when the task finishes successfully. This block has no return value and takes two arguments: the data task, and the response object created by the client response serializer.
    ///   - failure: A block object to be executed when the task finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the data task and the error describing the network or parsing error that occurred.
    /// - Returns: URLSessionDataTask
    @discardableResult
    func post(_ urlString: String,
              parameters: Any,
              success: @escaping SuccessCallback,
              failure: @escaping FailureCallback)
        -> URLSessionDataTask?
    {
        guard RunsNetIsReachable() else { return nil }
        let encodeParameters = encoder.encode(parameters, urlString)
        return sessionManager.post(urlString, parameters: encodeParameters, progress: nil, success: { [weak self] (task, respone) in
            guard let res = respone else {
                RunsMainThread {
                    failure(task, RunsError())
                }
                return
            }
            let json = RunsHttpObject.conversionAdaption(with:res)
            let map = self?.decoder.decode(json, urlString)
            RunsMainThread {
                success(task, map as Any)
            }
        }) { (task, error) in
            RunsMainThread {
                failure(task, error)
            }
        }
    }
    
    /// Creates and runs an `URLSessionDataTask` with a muRunsipart `POST` request.
    ///
    /// - Parameters:
    ///   - urlString: The URL string used to create the request URL.
    ///   - parameters: The parameters to be encoded according to the client request serializer.
    ///   - constructing: A block that takes a single argument and appends data to the HTTP body. The block argument is an object adopting the `AFMuRunsipartFormData` protocol.
    ///   - success: A block object to be executed when the task finishes successfully. This block has no return value and takes two arguments: the data task, and the response object created by the client response serializer.
    ///   - failure: A block object to be executed when the task finishes unsuccessfully, or that finishes successfully, but encountered an error while parsing the response data. This block has no return value and takes a two arguments: the data task and the error describing the network or parsing error that occurred.
    /// - Returns: URLSessionDataTask
    @discardableResult
    func post(_ urlString: String,
              parameters: Any,
              constructing: @escaping (AFMultipartFormData) ->Void,
              success: @escaping SuccessCallback,
              failure: @escaping FailureCallback)
        -> URLSessionDataTask?
    {
        guard RunsNetIsReachable() else { return nil }
        let encodeParameters = encoder.encode(parameters, urlString)
        return sessionManager.post(urlString, parameters: encodeParameters, constructingBodyWith: constructing, progress: nil, success: { [weak self] (task, respone) in
            guard let res = respone else {
                RunsMainThread {
                    failure(task, RunsError())
                }
                return
            }
            let json = RunsHttpObject.conversionAdaption(with:res)
            let map = self?.decoder.decode(json, urlString)
            RunsMainThread {
                success(task, map as Any)
            }
        }) { (task, error) in
            RunsMainThread {
                failure(task, error)
            }
        }
    }

}









