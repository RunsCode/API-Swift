//
//  RunsCallback.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import UIKit
private let DefaultInterval = 60
class RunsMirrorCallback<M>: NSObject, RunsResponeMirrorProtocol {
    
    typealias T = M
    var timeoutInterval: Int = DefaultInterval
    var uniqueId: UInt64 = 0
    //
    fileprivate var count: Int = 0
    fileprivate(set) var missionName: String = DefaultString
    //
    typealias SuccessdCallback = (M) -> Void
    typealias FailuredCallback = (M?, RunsErrorProtocol) -> Void
    private var successCallback: SuccessdCallback?
    private var failureCallback: FailuredCallback?
    deinit {
        RunsTerLog("Mirror uniqueId = \(uniqueId) release")
        queueTimer?.cancel()
        queueTimer = nil
    }
    
    init(unique id: UInt64, timeout interval: Int = DefaultInterval) {
        uniqueId = id
        timeoutInterval = interval
        RunsTerLog("Mirror uniqueId = \(uniqueId) init")
    }
    
    func success(_ res: M) {
        guard nil != successCallback else {
            return
        }
        stopMirror()
        RunsMainThread { [weak self] in
            self?.successCallback!(res)
            self?.successCallback = nil
            self?.failureCallback = nil
        }

    }
    
    func failure(_ res: M?, error: RunsErrorProtocol) {
        guard nil != failureCallback else {
            return
        }
        stopMirror()
        RunsMainThread { [weak self] in
            self?.failureCallback!(res,error)
            self?.successCallback = nil
            self?.failureCallback = nil
        }
    }
    
    func success(_ successed: @escaping SuccessdCallback, failure: @escaping FailuredCallback) {
        successCallback = successed
        failureCallback = failure
        startMirror()
    }
    
    fileprivate func establishTimer() {
        if nil == queueTimer || (queueTimer?.isCancelled)! {
            let queue = DispatchQueue(label: "com.socket_mirror.queue", attributes: .concurrent)
            queueTimer = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags.strict, queue: queue)
            queueTimer?.scheduleRepeating(deadline: DispatchTime.now(), interval: 1.0, leeway: .microseconds(10))
        }
    }
    
    fileprivate var queueTimer: DispatchSourceTimer?
}

extension RunsMirrorCallback {
    
    fileprivate func startMirror() {
        
        establishTimer()
        queueTimer?.setEventHandler { [weak self] in
            guard (self?.count)! < (self?.timeoutInterval)! else {
                self?.stopMirror()
                self?.callbackError()
                return
            }
            self?.count += 1
            RunsLog("滴滴 :\(self?.count ?? -1) ")
        }
        queueTimer?.resume()
    }
    
    fileprivate func stopMirror() {
        queueTimer?.cancel()
        queueTimer = nil
        self.count = 0
    }
    
    fileprivate func callbackError() {
        var error = RunsError(code: 1)
        error.description = "响应超时"
        failure(nil, error: error)
    }

}


















