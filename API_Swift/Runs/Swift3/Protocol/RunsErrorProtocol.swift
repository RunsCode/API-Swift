//
//  File.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation

protocol RunsErrorProtocol {
    var errorCode: Int {set get}
    var description: String {set get}
    var userInfo: [AnyHashable: Any] {set get}
}
