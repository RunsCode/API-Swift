//
//  RunsHttpRequestEncodeProtocol.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/7.
//  Copyright © 2017年 Giant. All rights reserved.
//

import UIKit

protocol RunsHttpRequestEncodeProtocol {
    func encode(_ parameters: Any, _ urlString: String) -> Any
}

extension RunsHttpRequestEncodeProtocol {
    
    func encode(_ parameters: Any, _ urlString: String = DefaultString) -> Any {
        guard let json = RunsHttpObject.convertJsonString(with: parameters)  else { return parameters }
        RunsTerLog("\n\n=============== Request: \(urlString) =============== \n\(json)\n============================== End ==============================")
        return parameters
    }
}
