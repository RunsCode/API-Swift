//
//  RunsHttpResponeDecodeProtocol.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/7.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation

protocol RunsHttpResponeDecodeProtocol {
    func decode(_ parameters: Any, _ urlString: String) -> Any
}

extension RunsHttpResponeDecodeProtocol {
    
    func decode(_ parameters: Any, _ urlString: String = DefaultString) -> Any {
        guard let json = RunsHttpObject.convertJsonString(with: parameters)  else { return parameters }
        RunsTerLog("\n\n=============== Respone: \(urlString) ===============\n\(json)\n============================== End ==============================")
        return parameters
    }

}
