//
//  RunsSocketResponeProtocol.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/4/5.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation


protocol RunsResponeMirrorProtocol {
    
    var timeoutInterval: Int {set get}
    var uniqueId: UInt64 {get}
    
    associatedtype T
    func success(_ res: T)
    func failure(_ res: T?, error: RunsErrorProtocol)
}


extension RunsResponeMirrorProtocol {
    
    func success(_ res: T) {
        
    }
    
    func failure(_ res: T?, error: RunsErrorProtocol) {
        
    }
}
