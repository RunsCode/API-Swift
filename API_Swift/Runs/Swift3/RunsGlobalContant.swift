//
//  RunsGlobalContant.swift
//  QiuQiuGram
//
//  Created by Dev_Wang on 2016/10/26.
//  Copyright © 2016年 iOS. All rights reserved.
//

import UIKit

typealias CancelCallbackFunc = () -> Void
typealias EventCancelCallbackFunc = () -> Bool
typealias ConditionCallbackFunc = (_ finished: Bool) -> Void
typealias IntentCallback = (_ idnex: Int) -> Void
typealias RequestCallbackFunc = (_ object: RunsHttpObject) -> Void
typealias BoolValueCallbackFunc = (_ success: Bool) -> Void
typealias PhotoActionCallbackFunc = (_ image: UIImage) -> Void
typealias VideoActionCallbackFunc = (URL?, Error?) -> Swift.Void


let SCREEN_RECT = UIScreen.main.bounds;
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let STATUS_BAR_HEIGHT = UIApplication.shared.statusBarFrame.size.height;
let NAVIGATIONBAR_HEIGHT: CGFloat = 64.0
let kSandDocumentPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!

//let PPCLIENT = RunsClient.getInstance() as! RunsClient

let DefaultZero = 0
let DefaultInt = -1
let DefaultString = ""
let DefaultDouble = -0.0000
let DefaultFLoat = -0.0


#if DEBUG
let IS_DATA_TEST = false
#else
let IS_DATA_TEST = false
#endif


class RunsGlobalContant: NSObject {

}
