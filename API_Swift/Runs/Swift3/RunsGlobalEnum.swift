//
//  RunsGlobalEnum.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/3/7.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation


enum ImageFormat: Int {
    case png    = 0
    case jpeg   = 1
    case bmp    = 2
    case gif    = 3
    
    init() {
        self = .png
    }
    
    var mime: String {
        get {
            switch self {
            case .png: return "image/png"
            case .jpeg: return "image/jpeg"
            case .bmp: return "image/bmp"
            case .gif: return "image/gif"
            }
        }
    }
    
    var format: String {
        get {
            switch self {
            case .png: return ".png"
            case .jpeg: return ".jpg"
            case .bmp: return ".bmp"
            case .gif: return ".gif"
            }
        }
    }
}

enum MultimediaFormat: Int {
    case mp3    = 0
    case amr    = 1
    case mp4    = 2
    case mpg4   = 3
    case m4v    = 4
    case mp4v   = 5
    
    init() {
        self = .mp3
    }
    
    var mime: String {
        get {
            switch self {
            case .mp3: return "audio/mpeg"
            case .amr: return "video/amr"
            case .mp4: return "video/mp4"
            case .mpg4: return "video/mp4"
            case .m4v: return "video/mp4"
            case .mp4v: return "video/mp4"
            }
        }
    }
    
    var format: String {
        get {
            switch self {
            case .mp3: return ".mp3"
            case .amr: return ".amr"
            case .mp4: return ".mp4"
            case .mpg4: return ".mp4"
            case .m4v: return ".mp4"
            case .mp4v: return ".mp4"
            }
        }
    }
}


enum TextFormat: Int {
    case plain  = 0
    case pdf    = 1
    case js     = 2
    case json   = 3
    case xml    = 4
    case md     = 5
    case doc    = 6
    case excel  = 7
    
    init() {
        self = .plain
    }
    
    var mime: String {
        get {
            switch self {
            case .plain: return "text/plain"
            case .pdf: return "application/pdf"
            case .js: return "application//javascript"
            case .json: return "application/json"
            case .xml: return "text/xml"
            case .md: return "application/octet-stream"
            case .doc: return "application/octet-stream"
            case .excel: return "application/octet-stream"
            }
        }
    }
    
    var format: String {
        get {
            switch self {
            case .plain: return ".txt"
            case .pdf: return ".pdf"
            case .js: return ".js"
            case .json: return ".json"
            case .xml: return ".xml"
            case .md: return ".md"
            case .doc: return ".doc"
            case .excel: return ".excel"
            }
        }
    }
}
