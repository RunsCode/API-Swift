//
//  RunsGlobalFunc.swift
//  QiuQiuGram
//
//  Created by Dev_Wang on 2016/10/25.
//  Copyright © 2016年 iOS. All rights reserved.
//

import UIKit
import AFNetworking

func RunsLog<T>(_ message: T, fileName: String = #file, methodName: String =  #function, lineNumber: Int = #line, thread: Thread = Thread.current) {
#if DEBUG
    let str : String = (fileName as NSString).pathComponents.last!.replacingOccurrences(of: "swift", with: "")
    NSLog("[Main Thread:\(thread.isMainThread)] \(str)\(methodName) \(lineNumber) : \(message)")
#else
#endif
}

func RunsTerLog(_ message: String, fileName: String = #file, methodName: String =  #function, lineNumber: Int = #line) {
    RunsLog(message, fileName: fileName, methodName: methodName, lineNumber: lineNumber)
//#if DEBUG
//    if IS_DATA_TEST {
//        PPCLIENT.sendNotification(kShowSocketJsonDataMessage, body: message)
//    }else{
//        RunsLog(message, fileName: fileName, methodName: methodName, lineNumber: lineNumber)
//    }
//#else
//#endif
}

//func RunsJson(_ message: Message, _ anyName: String = "") {
//#if DEBUG
//    let array = message.debugDescription.components(separatedBy: ".")[1].components(separatedBy: ":")
//    let text = message.textFormatString()
//    RunsTerLog("\n\n========================= \(array.first!) =========================\n\(text)=========================  End  ========================= \n\(anyName)\n\r  ")
//#else
//#endif
//}

func RunsAssert<T>(_ condition: Bool, _ messge: T, fileName: String = #file, methodName: String =  #function, lineNumber: Int = #line) {
#if DEBUG
    guard !condition else {
        //变量不符合条件判断时，执行下面代码
        return
    }
    RunsLog(messge)
    assert(condition)
#else
#endif
}

func RunsKindAssert<T>(obj: AnyObject, clazz: AnyClass, message: T, fileName: String = #file, methodName: String =  #function, lineNumber: Int = #line) -> Bool{
    let isKind = IsKinds(ofClass: clazz, obj: obj)
#if DEBUG
    RunsAssert(isKind, message)
    return isKind
#else
    return isKind
#endif
}

//func RunsDebugToast(_ message: String, _ interval: Float = 1.0, _ position: String = CSToastPositionCenter) {
//#if DEBUG
//    RunsMainThread {
//        (UIApplication.shared.delegate as! AppDelegate).window?.makeToast(message, duration: TimeInterval(interval), position: position)
//        RunsTerLog(message)
//    }
//#else
//#endif
//}

//func RunsReleaseToast(_ message: String, _ interval: Float = 1.0, _ position: String = CSToastPositionCenter) {
//    RunsMainThread {
//        (UIApplication.shared.delegate as! AppDelegate).window?.makeToast(message, duration: TimeInterval(interval), position: position)
//        RunsTerLog(message)
//    }
//}

func RunsTimeLoss<T>(_ operate: String = #function, _ callback: @escaping () -> T?) -> T? {
#if DEBUG
    let begin = Date().timeIntervalSince1970
    let result = callback()
    let end = Date().timeIntervalSince1970
    RunsLog("\(operate) 耗时: \(end - begin) 秒")
    return result
#else
    return callback()
#endif
}

func RunsMainThread(_ callback: @escaping () -> Void) {
    if Thread.isMainThread {
        callback()
        return
    }
    DispatchQueue.main.async {
        callback()
    }
}

func RunsNetIsReachable() -> Bool {
    guard AFNetworkReachabilityManager.shared().isReachable else {
        let alert = UIAlertView(title: "系统提示", message: "您的网络已经断开, 请确认您的网络是否正常", delegate: nil, cancelButtonTitle: "好的")
        alert.show()
        return AFNetworkReachabilityManager.shared().isReachable
    }
    return AFNetworkReachabilityManager.shared().isReachable
}

func RunsColor(_ HexRGB: Int) -> UIColor {
    return UIColor.init(colorLiteralRed: Float((HexRGB & 0xFF0000) >> 16)/255.0, green: Float((HexRGB & 0xFF00) >> 8)/255.0, blue: Float((HexRGB & 0xFF))/255.0, alpha: 1.0)
}

func RunsImage(view: UIImageView, insets: UIEdgeInsets) -> Void {
    view.image = view.image?.resizableImage(withCapInsets: insets)
}

func RunsButton(button: UIButton, insets: UIEdgeInsets) -> Void {
    let img0 = button.backgroundImage(for: .normal)
    let img1 = button.image(for: .highlighted)
    button.setBackgroundImage(img0?.resizableImage(withCapInsets: insets), for: .normal)
    button.setBackgroundImage(img1?.resizableImage(withCapInsets: insets), for: .highlighted)
}

//func RunsProxy(_ clazz: AnyClass) -> Proxy {
//    var proxy = RunsClient.getInstance().retrieveProxy(NSStringFromClass(clazz))
//    
//    if nil == proxy {
//        RunsLog("未发现 Proxy 进行热创建")
//        proxy = (clazz as! Proxy.Type).init()
//        let name = NSStringFromClass(clazz)
//        (proxy as! Proxy).proxyName = name
//        (proxy as! Proxy).initializeProxy()
//        PPCLIENT.register(proxy)
//    }
//    return proxy as! Proxy
//}

func IsEquals(ofString: String, str: String) -> Bool {
    return ofString == str
}

func IsKinds(ofClass: AnyClass, obj: AnyObject) -> Bool {
    return obj.isKind(of: ofClass)
}

func RANDOM_COLOR() -> UIColor? {
    let RANDOM_COLOR = UIColor.init(colorLiteralRed: Float(arc4random_uniform(255))/255, green: Float(arc4random_uniform(255))/255, blue: Float(arc4random_uniform(255))/255.0, alpha: 1)
    return RANDOM_COLOR
}

func RANDOM_DATA() -> NSString? {
    let RANDOM_DATA = NSString.init(format: "随机数据---%d", arc4random_uniform(1000000))
    return RANDOM_DATA
}

func CATransform3DMakePerspective(_ center: CGPoint, disZ: CGFloat) -> CATransform3D{
    let transToCenter = CATransform3DMakeTranslation(-center.x, -center.y, 0)
    let transBack = CATransform3DMakeTranslation(center.x, center.y, 0)
    var scale = CATransform3DIdentity
    scale.m34 = -1.0 / disZ
    
    return CATransform3DConcat(CATransform3DConcat(transToCenter, scale), transBack)
}

func CATransform3DPerspect(_ t: CATransform3D, center: CGPoint, disZ: CGFloat) -> CATransform3D {
    return CATransform3DConcat(t, CATransform3DMakePerspective(center, disZ: disZ))
}
