//
//  RunsGlogalMathFunc.swift
//  DriftingBottle-Swift
//
//  Created by Dev_Wang on 2017/2/23.
//  Copyright © 2017年 Giant. All rights reserved.
//

import Foundation

//MARK: Int to unsigned

func S_UInt(_ i: Int) -> UInt {
    if i < 0 {
        return 0
    }
    return UInt(i)
}

func S_UInt8(_ i: Int) -> UInt8 {
    if i < 0 {
        return 0
    }
    return UInt8(i)
}

func S_UInt16(_ i: Int) -> UInt16 {
    if i < 0 {
        return 0
    }
    return UInt16(i)
}

func S_UInt32(_ i: Int) -> UInt32 {
    if i < 0 {
        return 0
    }
    return UInt32(i)
}

func S_UInt64(_ i: Int) -> UInt64 {
    if i < 0 {
        return 0
    }
    return UInt64(i)
}

//MARK: String to Int

func S_Int(_ si: String) -> Int {
    
    guard let i: Int = Int(si) else {
        return 0
    }
    
    return Int(i)
}

func S_Int8(_ si: String) -> Int8 {
    
    guard let i: Int = Int(si) else {
        return 0
    }
    return Int8(i)
}

func S_Int16(_ si: String) -> Int16 {
    guard let i: Int = Int(si) else {
        return 0
    }
    return Int16(i)
}

func S_Int32(_ si: String) -> Int32 {
    guard let i: Int = Int(si) else {
        return 0
    }
    return Int32(i)
}

func S_Int64(_ si: String) -> Int64 {
    guard let i: Int = Int(si) else {
        return 0
    }
    return Int64(i)
}


//MARK: String to unsigned

func S_UInt(_ si: String) -> UInt {
    
    guard let i: Int = Int(si) else {
        return 0
    }
    
    return S_UInt(i)
}

func S_UInt8(_ si: String) -> UInt8 {
    
    guard let i: Int = Int(si) else {
        return 0
    }
    return S_UInt8(i)
}

func S_UInt16(_ si: String) -> UInt16 {
    guard let i: Int = Int(si) else {
        return 0
    }
    return S_UInt16(i)
}

func S_UInt32(_ si: String) -> UInt32 {
    guard let i: Int = Int(si) else {
        return 0
    }
    return S_UInt32(i)
}

func S_UInt64(_ si: String) -> UInt64 {
    guard let i: Int = Int(si) else {
        return 0
    }
    return S_UInt64(i)
}

//MARK: Double from Sin&Cos to CGFloat

func S_Sin(_ degrees: Double) -> CGFloat {
    return sin(CGFloat(degrees))
}

func S_Cos(_ degrees: Double) -> CGFloat {
    return cos(CGFloat(degrees))
}

//MARK: other

func StringToFloat(str: String)->(CGFloat) {
    if let doubleValue = Double(str) {
        return CGFloat(doubleValue)
    }
    return 0.0
}

func StringToInt(str: String)->(Int) {
    if let doubleValue = Int(str) {
        return Int(doubleValue)
    }
    return 0
}
